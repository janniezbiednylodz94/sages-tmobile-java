public class Konstruktory {
    public static void main(String[] args) {
        Punkt nowyPunkt = new Punkt(3, 4, true);
        System.out.println(nowyPunkt.wspolrzednaX);
        System.out.println(nowyPunkt.wspolrzednaY);

        Punkt staryPunkt = new Punkt();
        staryPunkt.wspolrzednaY = 4;
        staryPunkt.wspolrzednaX = 5;

        double odleglosc = nowyPunkt.obliczOdleglosc(staryPunkt);
        System.out.println(odleglosc);
    }
}
