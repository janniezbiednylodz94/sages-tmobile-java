import java.util.Objects;

public class Punkt
{
    double wspolrzednaX;
    double wspolrzednaY;

    Punkt() {}

    Punkt(double x, double y, boolean czyDziala)
    {
        this.wspolrzednaX = x;
        this.wspolrzednaY = y;

        System.out.println(czyDziala);
    }

    Punkt(double x, double y)
    {
        this.wspolrzednaX = x;
        this.wspolrzednaY = y;
    }

    Punkt(String x, String y)
    {
        this(Double.parseDouble(x), Double.parseDouble(y));
    }

    public double obliczOdleglosc(Punkt p)
    {
        double odlegloscX = this.wspolrzednaX - p.wspolrzednaX;
        double odlegloscY = this.wspolrzednaY - p.wspolrzednaY;

        return Math.sqrt(odlegloscX * odlegloscX + odlegloscY * odlegloscY);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Punkt punkt = (Punkt) o;
        return Double.compare(punkt.wspolrzednaX, wspolrzednaX) == 0 && Double.compare(punkt.wspolrzednaY, wspolrzednaY) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(wspolrzednaX, wspolrzednaY);
    }
}
