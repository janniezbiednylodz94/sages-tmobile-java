public class CiastoMain
{
    public static void main(String[] args) {
        Ciasto referencjaDoCiasta = new Ciasto();
        referencjaDoCiasta.iloscCukru = 100;
        referencjaDoCiasta.iloscKakao = 500;

        referencjaDoCiasta.gotuj();

        int wagaCiasta = referencjaDoCiasta.obliczWage();
        System.out.println(wagaCiasta);
    }
}
