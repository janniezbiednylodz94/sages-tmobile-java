public class Rzutowanie
{
    public static void main(String[] args)
    {
        int dzielna = 4;
        int dzielnik = 8;

        System.out.println("Wynik jako int:" + dzielna / dzielnik);

        double dzielnaD = 4;
        double dzielnikD = 8;

        System.out.println("Wynik jako double:" + dzielnaD / dzielnikD);

        int liczba = 4;
        double liczbaD = 8.5;

        int liczbaABC = (int) liczbaD;
        System.out.println("Rzutowanie liczby 8.5 na int" + liczbaABC);

        double liczbaK = liczba;
        System.out.println(liczbaK);
    }
}
