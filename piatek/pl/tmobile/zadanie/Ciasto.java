package pl.tmobile.zadanie;

/*
Napisz klase Ciasto.
Rozszerz ta klase o 2 konkretne rodzaje ciasta, np. sernik i brownie.
W tych klasach dodaj dowolne elementy, rozniace ja od ciasta.
Wykorzystaj mechanizm dziedziczenia i polimorfizmu, skomentuj swoj kod.
 */

public class Ciasto
{
    private String nazwaCiasta;

    public Ciasto(String nazwaCiasta) {
        this.nazwaCiasta = nazwaCiasta;
    }

    public String getNazwaCiasta() {
        return nazwaCiasta;
    }

    public void setNazwaCiasta(String nazwaCiasta) {
        this.nazwaCiasta = nazwaCiasta;
    }

    /**
     * Metoda usprawniajaca proces pieczenia
     */
    public void upieczMnie()
    {
        System.out.println("Proces pieczenia rozpoczety...");
    }
}
