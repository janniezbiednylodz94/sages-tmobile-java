package pl.tmobile.zadanie.ptaki;

public class Jaskolka
{
    private int wielkosc;

    public Jaskolka(int wielkosc) {
        this.wielkosc = wielkosc;
    }

    public int getWielkosc() {
        return wielkosc;
    }

    public void setWielkosc(int wielkosc) {
        this.wielkosc = wielkosc;
    }

    public void typowaJaskolka()
    {
        System.out.println("Jestem typowa jaskolka");
    }
}
