package pl.tmobile.zadanie.ptaki;

public class Koliber
{
    private int predkosc;

    public Koliber(int predkosc) {
        this.predkosc = predkosc;
    }

    public int getPredkosc() {
        return predkosc;
    }

    public void setPredkosc(int predkosc) {
        this.predkosc = predkosc;
    }

    public void typowyKolbier()
    {
        System.out.println("Typowy koliber");
    }
}
