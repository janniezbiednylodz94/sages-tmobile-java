package pl.tmobile.fukncyjne;

@FunctionalInterface
public interface WyrazeniaLambda<T, V> {
    boolean wyrazenie(T x, V y);
}
