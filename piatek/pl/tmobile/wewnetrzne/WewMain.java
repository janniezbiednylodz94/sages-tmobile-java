package pl.tmobile.wewnetrzne;

public class WewMain
{
    public static void main(String[] args) {
        ZewnetrznaKlasa.WewnetrznaKlasa ref2 = new ZewnetrznaKlasa.WewnetrznaKlasa();

        NowyPrzyklad nowyPrzyklad = new NowyPrzyklad() {
            @Override
            public void powitanie() {
                System.out.println("Przyklad");
            }
        };

        nowyPrzyklad.powitanie();
    }
}
