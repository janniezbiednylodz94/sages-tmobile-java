package pl.tmobile.wewnetrzne;

public class KlasaWewnetrzna
{
    public static void main(String[] args) {
        Lancuszek lancuszek = new Lancuszek();
        lancuszek.dodaj(1).dodaj(2).dodaj(3).dodaj(4);

        for(int i=0; i<4; i++)
        {
            System.out.println(lancuszek.pobierz());
        }

        System.out.println("Drugi lancuszek - tradycyjny");
        Lancuszek2 lancuszek2 = new Lancuszek2();
        lancuszek2.dodaj(13);
        lancuszek2.dodaj(14);
        lancuszek2.dodaj(15);

        while(lancuszek2.iloscKoralikow > 0)
        {
            System.out.println(lancuszek2.pobierz());
        }
    }
}
