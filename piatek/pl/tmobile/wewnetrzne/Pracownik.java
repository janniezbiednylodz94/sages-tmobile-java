package pl.tmobile.wewnetrzne;

import java.util.Date;

public class Pracownik
{
    private String imie;
    private String nazwisko;
    private Date dataZatrudnienia;

    public static class PracownikBuilder
    {
        private Pracownik nowy = new Pracownik();

        public PracownikBuilder dodajImie(String imie)
        {
            nowy.imie = imie;
            return this;
        }

        public PracownikBuilder dodajNazwisko(String nazwisko)
        {
            nowy.nazwisko = nazwisko;
            return this;
        }

        public PracownikBuilder dodajZatrudnienie(Date dataZatrudnienia)
        {
            nowy.dataZatrudnienia = dataZatrudnienia;
            return this;
        }

        public Pracownik zakoncz()
        {
            return nowy;
        }
    }

    @Override
    public String toString() {
        return "Pracownik{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", dataZatrudnienia=" + dataZatrudnienia +
                '}';
    }
}
