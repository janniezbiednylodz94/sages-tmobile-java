package pl.tmobile.dziedziczenie;

public class Auto
{
    private String marka;

    public Auto() {
    }

    public Auto(String marka) {
        this.marka = marka;
        System.out.println("Tworze auto");
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }
}
