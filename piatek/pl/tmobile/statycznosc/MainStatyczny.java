package pl.tmobile.statycznosc;

public class MainStatyczny
{
    public static void main(String[] args) {
        System.out.println(KlasaStatyczna.ktoryObiekt);
        KlasaStatyczna klasaStatyczna = new KlasaStatyczna("jeden");
        KlasaStatyczna klasaStatyczna2 = new KlasaStatyczna("dwa");
        System.out.println(klasaStatyczna.nazwa);
        System.out.println(klasaStatyczna.ktoryObiekt);

        System.out.println(klasaStatyczna2.nazwa);
        System.out.println(klasaStatyczna2.ktoryObiekt);

        klasaStatyczna.nazwa = "Nowa Jeden";
        klasaStatyczna2.zmienNazwe("Nowa Dwa");

        System.out.println("Po zmianie:");

        System.out.println(klasaStatyczna.nazwa);
        System.out.println(klasaStatyczna.ktoryObiekt);

        System.out.println(klasaStatyczna2.nazwa);
        System.out.println(klasaStatyczna2.ktoryObiekt);

        klasaStatyczna.ktoryObiekt = 30;
        System.out.println(klasaStatyczna2.ktoryObiekt);

        klasaStatyczna.nowaMetoda();
        klasaStatyczna2.nowaMetoda();

        KlasaStatyczna.nowaMetoda();
        System.out.println(KlasaStatyczna.ktoryObiekt);
    }
}
