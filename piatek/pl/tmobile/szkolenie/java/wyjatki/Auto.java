package pl.tmobile.szkolenie.java.wyjatki;

import java.io.Closeable;
import java.io.IOException;

public class Auto implements Closeable
{
    private double ladownosc;

    public Auto(double ladownosc) {
        this.ladownosc = ladownosc;
    }

    public double getLadownosc() throws MojPierwszyWyjatek
    {
        if(ladownosc < 1000)
        {
            throw new MojPierwszyWyjatek("Mam za malo ladownosci");
        }

        return ladownosc;
    }

    @Override
    public void close() throws IOException {
        System.out.println("Zamykam moj resource");
    }
}
