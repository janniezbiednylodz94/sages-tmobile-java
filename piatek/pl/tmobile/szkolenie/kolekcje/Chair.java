package pl.tmobile.szkolenie.kolekcje;

import java.util.Objects;

public class Chair
{
    private String model;
    private String manufacutrer;
    private int productionYear;

    public Chair(String model, String manufacutrer, int productionYear) {
        this.model = model;
        this.manufacutrer = manufacutrer;
        this.productionYear = productionYear;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacutrer() {
        return manufacutrer;
    }

    public void setManufacutrer(String manufacutrer) {
        this.manufacutrer = manufacutrer;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chair chair = (Chair) o;
        return productionYear == chair.productionYear && model.equals(chair.model) && manufacutrer.equals(chair.manufacutrer);
    }

    @Override
    public int hashCode() {
        return 17 * model.hashCode() + 31 * manufacutrer.hashCode() + 7 * productionYear;
    }
}
