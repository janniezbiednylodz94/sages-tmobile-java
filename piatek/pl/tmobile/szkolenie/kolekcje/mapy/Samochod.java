package pl.tmobile.szkolenie.kolekcje.mapy;

import java.util.Objects;

public class Samochod
{
    private String marka;

    public Samochod(String marka) {
        this.marka = marka;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Samochod samochod = (Samochod) o;
        return marka.equals(samochod.marka);
    }

    @Override
    public int hashCode() {
        return Objects.hash(marka);
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "marka='" + marka + '\'' +
                '}';
    }
}
