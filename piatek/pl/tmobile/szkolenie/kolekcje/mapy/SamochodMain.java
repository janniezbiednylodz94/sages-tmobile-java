package pl.tmobile.szkolenie.kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SamochodMain
{
    public static void main(String[] args) {
        Samochod audi = new Samochod("Audi");
        Samochod bmw = new Samochod("BMW");

        Map<String, Samochod> auta = new HashMap<String, Samochod>();

        auta.put("Audi", audi);
        auta.put("BMW", bmw);
        auta.put("Audi RS5", audi);

        System.out.println(auta.get("Audi RS5"));

        for(Map.Entry<String, Samochod> para : auta.entrySet())
        {
            System.out.println(para);
            System.out.println(para.getValue());
            System.out.println(para.getKey());
        }


        Properties p = new Properties();
        p.setProperty("A", "B");
        p.getProperty("database.hostname");

        /*
        database.hostname = localhost
        database.username = admin
        *
         */

//        Map<String, String> stringi = new HashMap<String, String>();
//        String przyklad = "A";
//
//        stringi.put("Pierwszy", przyklad);
//        stringi.put("Drugi", przyklad);
//
//        przyklad = "B";
//
//        /*
//        Pierwszy => A
//        Drugi => A
//         */
//
//        System.out.println(stringi.get("Pierwszy"));
//        System.out.println(stringi.get("Drugi"));
//
//        String a = stringi.get("Pierwszy");
//        // A = "A"
//        // a = "Inny Pierwszy"
//        // Pierwszy => A
//        // Drugi => A
//        a = "Inny Pierwszy";
//        //stringi.put("Pierwszy", a);
//        //stringi.put("Drugi", a);
//
//        System.out.println(stringi.get("Pierwszy"));
//        System.out.println(stringi.get("Drugi"));
//
//        stringi.remove("Pierwszy");
//        stringi.remove("Pierwszy", przyklad);
//        stringi.remove(null, przyklad);

        /*
        HashSet
        => implementuje intefejs Set - Set<String> a = new HashSet<String>()
        => trzyma obiekty, tylko uniklane
        => do dodawaniu add
        => hashCode generuje hash na podstawie elementow klasy

        HashMap
        => implementuje interfejs Map
        => pary obiektow w postaci <klucz, wartosc>
        => do dodawaniu put
        => hash pochodzi od wartosci z metody hashCode klucza
        => HashMap jest szybsza niz HashSet, poruszamy po unikatowych kluczach

        List => duplikaty, zachowana kolejnosc dodawania, przyklady ArrayList, LinkedList
        Set => bez duplikatow, dowolna kolejnosc, chyab ze korzystamy z np. TreeSet, przyklady HashSet, TreeSet
        Map => brak duplikatów kluczy, wiele kluczy do jednego obiektu, HashMapa
         */

        /*
        Napisz program, który będzie pobierał od użytkownika
        imiona par dopóki nie wprowadzi imienia „-”,
        następnie poproś użytkownika o podanie jednego z wcześniej
        wprowadzonych imion i wyświetl imię odpowiadającego mu partnera.
         */
    }
}
