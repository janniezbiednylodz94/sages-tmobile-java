package pl.tmobile.klasyabstrakcyjne;

public abstract class Pracownik
{
    private String imie;

    public Pracownik(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void wolajMnie()
    {
        System.out.println("Wolam!");
    }
}
