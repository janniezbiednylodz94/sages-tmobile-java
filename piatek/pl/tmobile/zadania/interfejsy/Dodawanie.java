package pl.tmobile.zadania.interfejsy;

public class Dodawanie implements Obliczenia
{

    @Override
    public double oblicz(double liczba1, double liczba2) {
        return liczba1 + liczba2;
    }
}
