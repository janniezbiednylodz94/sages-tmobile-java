package pl.tmobile.programy.sportowcy;

public class Pilkarz extends Sportowiec
{
    public Pilkarz(String imieSportowca) {
        super(imieSportowca);
    }

    @Override
    public double getPensja() {
        return 5000;
    }
}
