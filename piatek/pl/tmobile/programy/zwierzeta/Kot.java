package pl.tmobile.programy.zwierzeta;

public class Kot extends Zwierze
{
    public Kot(String imie) {
        super(imie);
    }

    @Override
    public void dajGlos()
    {
        System.out.println("Miau");
    }
}
