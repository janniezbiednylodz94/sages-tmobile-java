package pl.tmobile.programy.zwierzeta;

public class Krowa extends Zwierze
{
    public Krowa(String imie) {
        super(imie);
    }

    @Override
    public void dajGlos()
    {
        System.out.println("Muu");
    }
}
