package pl.tmobile.pl.tmobile.przesloniecie;

public class Kierownik extends Pracownik
{
    public Kierownik(String imie) {
        super(imie);
    }

    @Override
    public void powitanie()
    {
        super.powitanie();
        System.out.println("Czesc jestem kierownikiem");
    }
}
