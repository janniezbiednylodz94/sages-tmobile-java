package pl.tmobile.pl.tmobile.polimorfizm;

public class Chemia
{
    private String nazwa;

    public Chemia(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
