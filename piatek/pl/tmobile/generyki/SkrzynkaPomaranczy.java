package pl.tmobile.generyki;

public class SkrzynkaPomaranczy
{
    private Pomarancza pomarancza;

    public SkrzynkaPomaranczy() {}

    public SkrzynkaPomaranczy(Pomarancza pomarancza) {
        this.pomarancza = pomarancza;
    }

    public Pomarancza getPomarancza() {
        return pomarancza;
    }

    public void setPomarancza(Pomarancza pomarancza) {
        this.pomarancza = pomarancza;
    }
}
