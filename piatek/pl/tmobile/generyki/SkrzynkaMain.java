package pl.tmobile.generyki;

public class SkrzynkaMain
{
    public static void main(String[] args) {
//        SkrzynkaJablek skrzynkaJablek = new SkrzynkaJablek();
//        Jablko jablko = new Jablko();
//        skrzynkaJablek.setJablko(jablko);
//
//        SkrzynkaPomaranczy skrzynkaPomaranczy = new SkrzynkaPomaranczy();
//        Pomarancza pomarancza = new Pomarancza();
//        skrzynkaPomaranczy.setPomarancza(pomarancza);
//
//        SkrzynkaOwocow skrzynkaOwocow = new SkrzynkaOwocow();
//        Pomarancza pomarancza1 = new Pomarancza();
//        skrzynkaOwocow.setOwoc((Object) pomarancza1);

        SkrzynkaGeneryczna<Jablko> generyczneJablka = new SkrzynkaGeneryczna<Jablko>(new Jablko());
        Jablko jablko = new Jablko();
        generyczneJablka.setOwoc(jablko);

        Mail mail = new Mail("andrzej@miedzinski.net");
        String pobranyAdres = mail.getWartosc();
        System.out.println(pobranyAdres);

        Wiek wiek = new Wiek(18);
        Integer mojWiek = wiek.getWartosc();
        System.out.println(mojWiek);

        Wysokosc<Integer> wysokosc = new Wysokosc<Integer>(120);
        Integer mojaWysokosc = wysokosc.getWartosc();
        System.out.println(mojaWysokosc);

        Wieloparametrowa<String, Double> wieloparametrowa = new Wieloparametrowa<String, Double>("test", 3.3);
        System.out.println(wieloparametrowa.getInnyParametr());
        System.out.println(wieloparametrowa.getWartosc());

        SkrzynkaGeneryczna<Pomarancza> generycznePomarancze = new SkrzynkaGeneryczna<Pomarancza>(new Pomarancza());

        Pomarancza pomarancza = generycznePomarancze.getOwoc();
        System.out.println(pomarancza);

        //SkrzynkaGeneryczna<Marchewka>;




    }
}
