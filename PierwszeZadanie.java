public class PierwszeZadanie
{
    public static void main(String[] args) {
        // dwie liczby zmiennoprzecinkowe
        double liczbaA = 1.0;
        double liczbaB = 0.5;

        // dzialania
        double dodawanie = liczbaA + liczbaB;
        double odejmowanie = liczbaA - liczbaB;
        double mnozenie = liczbaA * liczbaB;
        double dzielenie = liczbaA / liczbaB;

        // sprawdzenie i wyswietlenie nieujemny i mniejszy od 2

        if((dodawanie >= 0) && (dodawanie < 2))
        {
            System.out.println("Dodawanie: " + dodawanie);
        }

        if((odejmowanie >= 0) && (odejmowanie < 2))
        {
            System.out.println("Odejmowanie: " + odejmowanie);
        }

        if((mnozenie >= 0) && (mnozenie < 2))
        {
            System.out.println("Mnozenie: " + mnozenie);
        }

        if((dzielenie >= 0) && (dzielenie < 2))
        {
            System.out.println("Dzielenie: " + dzielenie);
        }
    }
}
