package pl.tmobile.klasyabstrakcyjne;

public class Osobowe extends Auto
{
    public Osobowe(int dlugosc) {
        super(dlugosc);
    }

    @Override
    public double getCena()
    {
        return 0;
    }
}

/*
Typy proste
Napisz program, który policzy średnią z 3 dowolnych liczb.

Rzutowanie
Napisz program pobierający od użytkownika dwie liczby całkowite.
Wyświetl wynik ich dzielenia wraz z częścią ułamkową.

Pętle
Napisz program, który wyświetli malejąco wszystkie liczby od 20 do 10.
 */