package pl.tmobile.klasyabstrakcyjne;

public class Abstrakcja
{
    public static void main(String[] args) {
        Pracownik pracownik;

        Kierownik kierownik = new Kierownik("Jan");

        Pracownik pracownik1 = new Kierownik("Tomasz");
        pracownik1.wolajMnie();

        Auto car = new Osobowe(2);
        Osobowe osobowe = new Osobowe(2);

        Auto a = new Auto(3) {
            @Override
            public double getCena() {
                return 0;
            }
        };
    }
}
