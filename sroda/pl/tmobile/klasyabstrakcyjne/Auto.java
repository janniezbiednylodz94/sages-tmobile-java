package pl.tmobile.klasyabstrakcyjne;

public abstract class Auto
{
    private int dlugosc;

    public Auto(int dlugosc) {
        this.dlugosc = dlugosc;
    }

    public int getDlugosc() {
        return dlugosc;
    }

    public void setDlugosc(int dlugosc) {
        this.dlugosc = dlugosc;
    }

    public abstract double getCena();

    public void nowa()
    {

    }
}
