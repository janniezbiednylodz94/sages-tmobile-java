package pl.tmobile.programy.sportowcy;

public abstract class Sportowiec
{
    private String imieSportowca;

    public Sportowiec(String imieSportowca) {
        this.imieSportowca = imieSportowca;
    }

    public String getImieSportowca() {
        return imieSportowca;
    }

    public void setImieSportowca(String imieSportowca) {
        this.imieSportowca = imieSportowca;
    }

    public abstract double getPensja();
}
