package pl.tmobile.programy.sportowcy;

public class Koszykarz extends Sportowiec
{
    public Koszykarz(String imieSportowca) {
        super(imieSportowca);
    }

    @Override
    public double getPensja() {
        return 3000;
    }
}
