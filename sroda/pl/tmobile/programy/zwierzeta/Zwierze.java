package pl.tmobile.programy.zwierzeta;

public class Zwierze
{
    private String imie;

    public Zwierze(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void dajGlos()
    {
        System.out.println("Domyslny glos");
    }
}
