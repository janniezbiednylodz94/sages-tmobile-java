package pl.tmobile.programy;

import java.util.Scanner;

public class Proste
{
    public static void main(String[] args) {
        /*
        Napisz program, który policzy średnią z 3 dowolnych liczb.
         */

        System.out.println(obliczSrednia(5, 8, 10));

        /*
        Rzutowanie
        Napisz program pobierający od użytkownika dwie liczby całkowite.
        Wyświetl wynik ich dzielenia wraz z częścią ułamkową.
         */

        Scanner scanner = new Scanner(System.in);
        int liczbaA = scanner.nextInt();
        int liczbaB = scanner.nextInt();

        System.out.println(wykonajDzielenie(liczbaA, liczbaB));

        /*
        Pętle
        Napisz program, który wyświetli malejąco wszystkie liczby od 20 do 10.
         */

        wypiszMalejaco(20, 10);
        wypiszMalejaco(30, 10);
        wypiszMalejaco(20, 10);
        wypiszMalejaco(20, 10);
    }

    public static void wypiszMalejaco(int odJakiej, int doJakiej)
    {
        for(int wartoscStartowa = odJakiej; wartoscStartowa >= doJakiej; wartoscStartowa--)
        {
            System.out.println(wartoscStartowa);
        }
    }

    public static double wykonajDzielenie(int liczbaA, int liczbaB)
    {
        return (double) liczbaA / (double) liczbaB;
    }

    public static double obliczSrednia(int a, int b, int c)
    {
        return (a + b + c) / 3;
    }
}
