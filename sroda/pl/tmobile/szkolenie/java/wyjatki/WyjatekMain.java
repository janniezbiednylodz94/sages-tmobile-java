package pl.tmobile.szkolenie.java.wyjatki;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class WyjatekMain
{
    public static void main(String[] args) {
        //ileSekundWGodzinach(-1);

        /*int iloscGodzin = -1;
        int iloscSekund = 0;

        try
        {
            iloscSekund = ileSekundWGodzinach(iloscGodzin);
        }
        catch (IllegalArgumentException exception)
        {
            iloscSekund = ileSekundWGodzinach(-1 * iloscGodzin);
        }

        System.out.println(iloscSekund);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String data = "12-12-2022";
        Date sformatowanaData = null;

        try
        {
            sformatowanaData = sdf.parse(data);
        }
        catch(ParseException pe)
        {
            pe.printStackTrace();
        }

        System.out.println(sformatowanaData);

        Auto auto = new Auto(1200);

        try
        {
            System.out.println(auto.getLadownosc());
        }
        catch (MojPierwszyWyjatek mpw)
        {
            mpw.printStackTrace();
        }

        FileOutputStream fos = null;

        try
        {
            fos = new FileOutputStream(new File("nowyPlik.txt"));
        }
        catch(FileNotFoundException fnfe)
        {
            fnfe.printStackTrace();
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        try(FileOutputStream fos2 = new FileOutputStream(new File("plik.txt"));)
        {
            fos2.write(null);
        }
        catch(FileNotFoundException fnfe)
        {
            fnfe.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /*
        Napisz program, który pobierze od użytkownika liczbę i wyświetli
        jej pierwiastek. Do obliczenia pierwiastka możesz użyć istniejącej
        metody java.lang.Math.sqrt(). Jeśli użytkownik poda liczbę
        ujemną rzuć wyjątek java.lang.IllegalArgumentException.
        Obsłuż sytuację, w której użytkownik poda ciąg znaków, który nie
        jest liczbą.
         */



    }

    public static int ileSekundWGodzinach(int iloscGodzin) throws IllegalArgumentException {
        if(iloscGodzin < 0)
        {
            throw new IllegalArgumentException("Ilosc godzin musi byc wieksza od 0 a ma: " + iloscGodzin);
        }

        return iloscGodzin * 60 * 60;
    }
}
