package pl.tmobile.szkolenie.java.wyjatki;

public class MojPierwszyWyjatek extends Exception
{
    public MojPierwszyWyjatek(String message)
    {
        super(message);
    }
}
