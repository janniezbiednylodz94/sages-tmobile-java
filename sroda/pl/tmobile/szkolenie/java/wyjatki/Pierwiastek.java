package pl.tmobile.szkolenie.java.wyjatki;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Pierwiastek
{
    /*
        Napisz program, który pobierze od użytkownika liczbę i wyświetli
        jej pierwiastek. Do obliczenia pierwiastka możesz użyć istniejącej
        metody java.lang.Math.sqrt(). Jeśli użytkownik poda liczbę
        ujemną rzuć wyjątek java.lang.IllegalArgumentException.
        Obsłuż sytuację, w której użytkownik poda ciąg znaków, który nie
        jest liczbą.
         */

    public static void main(String[] args)
    {
        double pobranaLiczba = 0;

        Scanner scanner = new Scanner(System.in);
        try
        {
            pobranaLiczba = scanner.nextDouble();
        }
        catch(InputMismatchException e)
        {
            e.printStackTrace();
        }

        if(pobranaLiczba < 0)
        {
            throw new IllegalArgumentException("Podana liczba jest ujemna");
        }

        System.out.println(Math.sqrt(pobranaLiczba));

    }
}
