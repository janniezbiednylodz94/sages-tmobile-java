package pl.tmobile.szkolenie.kolekcje;

import java.util.ArrayList;

public class Kolekcja
{
    public static void main(String[] args)
    {
        String tekst = "Andrzej";
        ArrayList<String> arrayListaStringow = new ArrayList<String>();
        arrayListaStringow.add(tekst);
        arrayListaStringow.add("ma");
        arrayListaStringow.add("szkolenie");

        ArrayList<String> kopiaPowyzszejArraylist = new ArrayList<>(arrayListaStringow);

        for(String s : kopiaPowyzszejArraylist)
        {
            arrayListaStringow.remove(s);
        }

//        for(int i = 0; i < arrayListaStringow.size(); i++)
//        {
//            System.out.println(arrayListaStringow.get(i));
//        }


    }
}
