package pl.tmobile.zadanie;

public class Brownie extends Ciasto
{
    private int iloscCzekolady;

    public Brownie(String nazwaCiasta, int iloscCzekolady) {
        super(nazwaCiasta);
        this.iloscCzekolady = iloscCzekolady;
    }

    public int getIloscCzekolady() {
        return iloscCzekolady;
    }

    public void setIloscCzekolady(int iloscCzekolady) {
        this.iloscCzekolady = iloscCzekolady;
    }

    public int zwiekszIloscCzekolady(int mnoznik)
    {
        int nowaIlosc = this.iloscCzekolady * mnoznik;
        this.iloscCzekolady = nowaIlosc;
        return nowaIlosc;
    }
}
