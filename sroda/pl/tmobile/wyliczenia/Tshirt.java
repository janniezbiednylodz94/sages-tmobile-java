package pl.tmobile.wyliczenia;

public class Tshirt {

    private TshirtSize size;
    private String manufacturer;

    public Tshirt(TshirtSize size, String manufacturer)
    {
        this.size = size;
        this.manufacturer = manufacturer;
    }

    public static void main(String[] args) {

        Tshirt tshirt = new Tshirt(TshirtSize.L, "Polska");
        System.out.println(tshirt.size.getDlugosc());

        TshirtSize.values();

        System.out.println(TshirtSize.XL.name());

        if(TshirtSize.S.equals(TshirtSize.S))
        {
            System.out.println("True");
        }

        switch(tshirt.size)
        {
            case S:
                System.out.println("S");
                break;

            case L:
                System.out.println("L");
                break;
        }


    }
}
