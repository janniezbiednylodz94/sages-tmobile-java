package pl.tmobile.wrappery;

public class Wrapper {

    public static void main(String[] args)
    {
        int zmiennaInt = 3;
        float zmiennaFloat = 3;

        Integer zmiennaInteger = 3;
        System.out.println(zmiennaInteger.intValue());

        int maxValue = Integer.MAX_VALUE;
        System.out.println(maxValue);

        int podstawowyInt = Integer.valueOf(123);
        long podstawowyLong = Long.valueOf(123L);

        Integer test1 = 123;
        Long longtest = 123L;

        String testy = "123" + test1;
        System.out.println(testy);

        String testyp = longtest + "123";
        System.out.println(testyp);

        String z = "Asia ma lat: " + test1;
        System.out.println(z);

    }
}
