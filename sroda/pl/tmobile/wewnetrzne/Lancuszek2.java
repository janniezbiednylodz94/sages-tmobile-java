package pl.tmobile.wewnetrzne;

public class Lancuszek2
{
    int iloscKoralikow;
    Koralik2 pierwszyKoralik;

    public Lancuszek2() {}

    public int getIloscKoralikow() {
        return iloscKoralikow;
    }

    public void dodaj(int wartoscKoralika)
    {
        Koralik2 nowyKoralik = new Koralik2(wartoscKoralika);
        nowyKoralik.setNastepnyKoralik(pierwszyKoralik);
        pierwszyKoralik = nowyKoralik;
        iloscKoralikow++;
    }

    public int pobierz()
    {
        Koralik2 koralik2 = pierwszyKoralik;
        pierwszyKoralik = pierwszyKoralik.getNastepnyKoralik();
        iloscKoralikow--;
        return koralik2.getWartoscKoralika();
    }

}
