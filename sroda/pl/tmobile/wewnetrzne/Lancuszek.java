package pl.tmobile.wewnetrzne;

public class Lancuszek
{
    private Koralik pierwszy;

    private static class Koralik
    {
        private int wartosc;
        private Koralik nastepny;

        private Koralik(int wartosc, Koralik nastepnyKoralik)
        {
            this.wartosc = wartosc;
            this.nastepny = nastepnyKoralik;
        }
    }

    public Lancuszek dodaj(int wartosc)
    {
        pierwszy = new Koralik(wartosc, pierwszy);
        return this;
    }

    public int pobierz()
    {
        int x = pierwszy.wartosc;
        pierwszy = pierwszy.nastepny;
        return x;
    }
}
