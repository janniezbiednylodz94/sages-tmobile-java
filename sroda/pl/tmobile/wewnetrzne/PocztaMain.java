package pl.tmobile.wewnetrzne;

import java.util.Date;

public class PocztaMain
{
    public static void main(String[] args) {
        Poczta poczta = new Poczta();
        poczta.wyslijDoJednejOsoby();
        poczta.rozpocznijSpamowanie();

        Poczta poczta1 = new Poczta();
        Poczta.Newsletter newsletter = poczta1.new Newsletter();
        poczta1.rozpocznijSpamowanie();

        Pracownik pracownik = new Pracownik.PracownikBuilder()
                .dodajImie("Andrzej").zakoncz();

        System.out.println(pracownik);
    }
}
