package pl.tmobile.interfejsy;

public class Interfejs
{

    public static void main(String[] args) {
        Punkt p = new Punkt(3, 5);
        System.out.println(p.getX());
        p.policzPromocje();
        p.znizka();

        Promocyjny promocyjny = new Punkt(3, 4);
        System.out.println(promocyjny.policzPromocje());

    }

}
