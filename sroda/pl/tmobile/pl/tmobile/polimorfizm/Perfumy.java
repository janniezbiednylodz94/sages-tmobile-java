package pl.tmobile.pl.tmobile.polimorfizm;

public class Perfumy extends Chemia
{
    private int pojemnosc;

    public Perfumy(String nazwa, int pojemnosc) {
        super(nazwa);
        this.pojemnosc = pojemnosc;
    }

    public int getPojemnosc() {
        return pojemnosc;
    }

    public void setPojemnosc(int pojemnosc) {
        this.pojemnosc = pojemnosc;
    }
}
