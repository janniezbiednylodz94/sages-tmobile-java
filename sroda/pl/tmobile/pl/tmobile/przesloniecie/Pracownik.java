package pl.tmobile.pl.tmobile.przesloniecie;

public class Pracownik
{
    private String imie;

    public Pracownik(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void powitanie()
    {
        System.out.println("Czesc jestem pracownikiem");
    }
}
