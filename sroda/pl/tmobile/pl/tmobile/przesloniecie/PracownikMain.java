package pl.tmobile.pl.tmobile.przesloniecie;

public class PracownikMain {

    public static void main(String[] args) {
        Pracownik pracownik = new Pracownik("Andrzej");
        pracownik.powitanie();

        Kierownik kierownik = new Kierownik("Andrzej");
        kierownik.powitanie();

        IT it = new IT("Jan");
        it.powitanie();

        Pracownik referencje = null;
        int rodzaj = 1;

        if(rodzaj == 1)
        {
            referencje = new Kierownik("Pawel");
        }
        else if(rodzaj == 2)
        {
            referencje = new IT("Jan");
        }

        System.out.println("Wynik:");
        referencje.powitanie();

        Kierownik kierownik2 = new Kierownik("A");
        kierownik2.powitanie();
    }
}

/*
Napisz klase Ciasto.
Rozszerz ta klase o 2 konkretne rodzaje ciasta, np. sernik i brownie.
W tych klasach dodaj dowolne elementy, rozniace ja od ciasta.
Wykorzystaj mechanizm dziedziczenia i polimorfizmu, skomentuj swoj kod.
 */