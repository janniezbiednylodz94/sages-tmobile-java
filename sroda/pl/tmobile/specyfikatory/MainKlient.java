package pl.tmobile.specyfikatory;

public class MainKlient
{
    public static void main(String[] args) {

        // Tworzenie obiektow klasy klient
        Klient klient1 = new Klient();
        Klient klient2 = new Klient("Andrzej", 123L);

        /*
        Komentarz
        ktory
        zawiera
        wiele
        linii
         */
        System.out.println(klient1);
        System.out.println(klient2);

        /**/


        klient2.setImie("Magda");

        System.out.println(klient2);

        //klient1.tworzeObiekt();


        //klient2.tworzeObiekt();

        //

        Klasa.nicNieRobie();
    }
}
