package pl.tmobile.dziedziczenie;

public class Dostawcze extends Auto
{
    private int szerokosc;

    public Dostawcze(int szerokosc) {
        this.szerokosc = szerokosc;
    }

    public Dostawcze(String marka, int szerokosc) {
        super(marka);
        this.szerokosc = szerokosc;
    }

    public int getSzerokosc() {
        return szerokosc;
    }

    public void setSzerokosc(int szerokosc) {
        this.szerokosc = szerokosc;
    }
}
