public class Klasa
{
    public static void main(String[] args)
    {
        int liczba = 3;

        Punkt p1 = new Punkt();
        p1.wspolrzednaX = 3;
        p1.wspolrzednaY = 5;

        System.out.println("Punkt o wspolrzednych: " + p1.wspolrzednaX + " " + p1.wspolrzednaY);

        Punkt p2 = new Punkt();
        p2.wspolrzednaX = 3;
        p2.wspolrzednaY = 5;

        System.out.println(p1);
        System.out.println(p2);

        System.out.println("Punkt o wspolrzednych: " + p2.wspolrzednaX + " " + p2.wspolrzednaY);

        double odlegloscMiedzyPunktami = p1.obliczOdleglosc(p2);
        System.out.println("Odleglosc: " + odlegloscMiedzyPunktami);

    }
}
