//Napisz program, który za pomocą pętli wpisze do
//        tablicy wartości tabliczki mnożenia do 100.
//        Sprawdź za pomocą pętli for każdy
//        element i wyświetl tylko te,
//        których reszta z dzielenia przez 10 jest równa 0.

public class DrugieZadanie
{
    public static void main(String[] args) {
        int[][] tabliczkaMnozenia = new int[11][11];

        for(int i = 1; i < tabliczkaMnozenia.length; i++)
        {
            for(int j = 1; j < tabliczkaMnozenia[i].length; j++)
            {
                tabliczkaMnozenia[i][j] = i * j;
                System.out.format("%4d", + tabliczkaMnozenia[i][j]);
            }
            System.out.println("");
        }

        System.out.println("Reszta:");
        for(int i = 1; i < tabliczkaMnozenia.length; i++)
        {
            for(int j = 1; j < tabliczkaMnozenia[i].length; j++)
            {
                if(tabliczkaMnozenia[i][j] % 10 == 0)
                {
                    System.out.format("%4d", tabliczkaMnozenia[i][j]);
                }
            }
            System.out.println("");
        }
    }
}
