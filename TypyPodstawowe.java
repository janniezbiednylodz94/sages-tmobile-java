public class TypyPodstawowe
{
    public static void main(String[] args) {
        int pudelkoIntow = 20; // liczby calkowita
        char pudelkoLiterka = 'a'; // pojedynczy znak

        long pudelkoLong = 999999999999999L; // duze liczby
        byte pudelkoByte = 1; // bajty
        boolean pudelkoBoolean = true; // wartosci logiczne true lub false

        float pudelkoFloat = 3.14f; // zmiennoprzecinkowe
        double pudelkoDouble = 3.14; // zmiennoprzecinkowe - duza precyzja

        System.out.println("Czy jestem stary? " + pudelkoBoolean);
    }
}
