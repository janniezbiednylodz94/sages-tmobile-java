public class Petle
{
    public static void main(String[] args)
    {
        int liczba = 1;

        System.out.println(liczba++); // 1, a potem zwiekszam do 2
        System.out.println(++liczba); // 2, wiec zwiekszam do 3 i wyswietlam

        // Najpierw pobierz, potem zwieksz lub zmniejsz
        // liczba++ - postinkrementacja i = i + 1
        // liczba-- -postdekrementacja i = i - 1;

        // Najpierw zmniejsz lub zwieksz a potem uzyj
        //++liczba - preinkrementacji
        //--liczba - predekrementacji

        int liczbaStartowa = 1;

        do
        {
            System.out.println(liczbaStartowa);
            liczbaStartowa++;
        }
        while(liczbaStartowa <=10);

        int drugaLiczba = 10;

        while(drugaLiczba > 0)
        {
            System.out.println(drugaLiczba);
            drugaLiczba--;
        }

//        for(wyrazenie typu void; wyrazenie typu boolean; wyrazenie typu void)
//        {
//
//        }
//
//        for(warunekstartowy; warunekJakDlugo; oIleZwiekszam)

        for(int i = 0; i < 5; i++)
        {
            System.out.println("For: " + i);
        }

        for(int i = 0; i < 5;)
        {
            System.out.println("For 2: " + i);
            i++;
        }

        int z = 0;

        for(; z < 5;)
        {
            System.out.println("For 3: " + z);
            z++;
        }

        while(true)
        {
            System.out.println("Nieskonczona petla");
        }


    }
}
