package pl.tmobile.pl.tmobile.polimorfizm;

public class PolimorfizmMain
{
    public static void main(String[] args) {
        Chemia chemia = new Chemia("proszek");
        Perfumy perfumy = new Perfumy("ck", 100);

        System.out.println(chemia.getNazwa());
        System.out.println(perfumy.getNazwa());

        Chemia chemiaNowa = new Perfumy("th", 200);
    }
}
