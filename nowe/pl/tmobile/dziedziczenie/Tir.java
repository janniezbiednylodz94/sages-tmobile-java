package pl.tmobile.dziedziczenie;

public class Tir extends Auto
{
    private int ladownosc;

    Tir(String marka)
    {
        super(marka);
    }

    Tir(String marka, int ladownosc)
    {
        super(marka);
        this.ladownosc = ladownosc;
        System.out.println("Tworze Tira");
    }

    public int getLadownosc() {
        return ladownosc;
    }

    public void setLadownosc(int ladownosc) {
        this.ladownosc = ladownosc;
    }
}
