package pl.tmobile.signleton;

public class Punkt
{
    private static Punkt instancja;

    private Punkt() {}

    public static Punkt getInstancja()
    {
        if(instancja == null)
        {
            instancja = new Punkt();
        }

        return instancja;
    }
}
