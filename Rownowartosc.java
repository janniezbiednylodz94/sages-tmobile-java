public class Rownowartosc
{
    public static void main(String[] args) {
        int liczba = 3;
        int liczba2 = 5;

        if(liczba == liczba2)
        {
            System.out.println("Tak");
        }
        else
        {
            System.out.println("Nie");
        }

        String tekst = "tekst";
        String tekst2 = new String("tekst");

        System.out.println(tekst == tekst2); // zle
        System.out.println(tekst.equals(tekst2));

        Punkt p1 = new Punkt();
        p1.wspolrzednaY = 3;
        p1.wspolrzednaX = 5;

        Punkt p2 = new Punkt();
        p2.wspolrzednaY = 3;
        p2.wspolrzednaX = 5;

        System.out.println(p1 == p2); // zle
        System.out.println(p1.equals(p2));


    }
}
