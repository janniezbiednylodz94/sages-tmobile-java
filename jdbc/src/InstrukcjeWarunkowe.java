import java.util.Scanner;

public class InstrukcjeWarunkowe
{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int liczbaKlawiatury = scanner.nextInt();

        if(liczbaKlawiatury == 5)
        {
            System.out.println("Prawda 5");
        }
        else if(liczbaKlawiatury == 4)
        {
            System.out.println("Prawda 4");
        }
        else
        {
            System.out.println("Cos innego");
        }
        
        switch(liczbaKlawiatury)
        {
            case 5:
                System.out.println("Piec");
                break;

            case 10:
                System.out.println("Jestem dziesiatka");
                break;

            default:
                System.out.println("Jestem czyms innym");
        }
    }
}
