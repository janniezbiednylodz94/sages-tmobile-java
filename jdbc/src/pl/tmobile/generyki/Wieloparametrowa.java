package pl.tmobile.generyki;

public class Wieloparametrowa<T, E> extends AbstrakcjaGeneryczna<E>
{
    private T innyParametr;

    public Wieloparametrowa(T innyParametr, E wartosc) {
        super(wartosc);
        this.innyParametr = innyParametr;
    }

    public T getInnyParametr() {
        return innyParametr;
    }

    public void setInnyParametr(T innyParametr) {
        this.innyParametr = innyParametr;
    }
}
