package pl.tmobile.generyki;

import java.util.ArrayList;
import java.util.List;

public class WlasnaLista
{
    public <T> List<T> makeList(T t1, T t2)
    {
        List<T> lista = new ArrayList<T>();
        lista.add(t1);
        lista.add(t2);
        return lista;
    }

    public void uzyj()
    {
        List<String> listaStringow = makeList("Andrzej", "Andrzej");
        List<Integer> listaIntegerow = makeList(1,2);
    }
}
