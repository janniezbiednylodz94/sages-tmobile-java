package pl.tmobile.generyki;

public class SkrzynkaJablek
{
    private Jablko jablko;

    public SkrzynkaJablek() {}

    public SkrzynkaJablek(Jablko jablko) {
        this.jablko = jablko;
    }

    public Jablko getJablko() {
        return jablko;
    }

    public void setJablko(Jablko jablko) {
        this.jablko = jablko;
    }
}
