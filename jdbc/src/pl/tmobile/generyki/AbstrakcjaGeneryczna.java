package pl.tmobile.generyki;

public abstract class AbstrakcjaGeneryczna<T>
{
    private T wartosc;

    public AbstrakcjaGeneryczna(T wartosc) {
        this.wartosc = wartosc;
    }

    public T getWartosc() {
        return wartosc;
    }

    public void setWartosc(T wartosc) {
        this.wartosc = wartosc;
    }
}
