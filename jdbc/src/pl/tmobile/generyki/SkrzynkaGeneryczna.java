package pl.tmobile.generyki;

public class SkrzynkaGeneryczna<T extends Owoc>
{
    public T owoc;

    public SkrzynkaGeneryczna(T owoc) {
        this.owoc = owoc;
    }

    public T getOwoc() {
        return owoc;
    }

    public void setOwoc(T owoc) {
        this.owoc = owoc;
    }
}
