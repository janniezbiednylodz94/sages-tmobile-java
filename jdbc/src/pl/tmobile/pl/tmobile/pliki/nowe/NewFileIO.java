package pl.tmobile.pl.tmobile.pliki.nowe;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class NewFileIO
{
    public static void main(String[] args) {
        Path katalog = Paths.get("/var/www");
        Path nazwaPliku = Paths.get("tmobile.txt");
        Path windows = Paths.get("C:\\katalog\\plik.txt");
        Path polaczone = katalog.resolve(nazwaPliku);
        System.out.println(windows.getFileName());

        System.out.println(Files.exists(nazwaPliku));
        System.out.println(Files.notExists(nazwaPliku));

        //Files.isDirectory()
        //Files.isRegularFile();
        //Files.isExecutable()

        Path tmobile = Paths.get("tmobile.txt");
        try {
            byte[] dane = Files.readAllBytes(tmobile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            List<String> linie = Files.readAllLines(tmobile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> linie = Arrays.asList(new String[] { "aaa", "bbb", "ccc"});
        try {
            Files.write(tmobile, linie);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
