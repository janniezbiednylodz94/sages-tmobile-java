package pl.tmobile.specyfikatory;

public class Klient
{
    private String imie;
    private Long pesel;

    public Klient() {};

    public Klient(String imie, Long pesel)
    {
        this.imie = imie;
        this.pesel = pesel;
        this.tworzeObiekt();
    }

    @Override
    public String toString() {
        return "Klient{" +
                "imie='" + imie + '\'' +
                ", pesel=" + pesel +
                '}';
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    private static void tworzeObiekt()
    {
        System.out.println("Tworze obiekt");
    }

}
