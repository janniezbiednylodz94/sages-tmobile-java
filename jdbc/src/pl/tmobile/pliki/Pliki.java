package pl.tmobile.pliki;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Pliki
{
    public static void main(String[] args) {
        File f = new File("test.txt");
        System.out.println(f.exists());
        System.out.println(f.isFile());
        System.out.println(f.getAbsolutePath());

        /*
            C:\\katalog\\plik.txt
            home/usr/andrzej/katalog/plik.txt

               */



        //new BufferedReader(new FileReader(new File("sciezka/do/pliku/plik.txt")));
        //new BufferedReader(new FileReader("sciezka/tutaj"));

//        Scanner scanner = new Scanner(System.in);
//        File file = new File(scanner.next());
//
//        try {
//            if(file.createNewFile())
//            {
//                System.out.println("Plik utworzony");
//            }
//            else
//            {
//                System.out.println("Plik istnieje");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        File file1 = new File("tmobile.txt");
//        FileWriter out = null;
//
//        try {
//            out = new FileWriter(file1);
//            file1.createNewFile();
//            out.write("Witaj w moim pliku");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        finally {
//            try {
//                out.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

//        File file = new File("tmobile2.txt");
//        try(PrintWriter out = new PrintWriter(new FileWriter(file)))
//        {
//            file.createNewFile();
//            out.write("Test \t Test \n");
//            out.write("Test \n");
//            out.println("Test");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        File file = new File("tmobile2.txt");
//        try(BufferedReader in = new BufferedReader(new FileReader(file)))
//        {
//            String line;
//
//            while((line = in.readLine()) != null)
//            {
//                System.out.println(line);
//            }
//        }
//        catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } ;

//        File doOdczytu = new File("tmobile.txt");
//        File doZapisu = new File("tmobile3.txt");
//
//        try(FileInputStream fis = new FileInputStream(doOdczytu);
//        FileOutputStream fos = new FileOutputStream(doZapisu);)
//        {
//            doZapisu.createNewFile();
//            byte[] data = new byte[fis.available()];
//            fis.read(data);
//            fos.write(data);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        List<Samochod> lista = new ArrayList<Samochod>();
//
//        File file = new File("auta.o");
//
//        try
//        {
//            file.createNewFile();
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//
//        Scanner scanner = new Scanner(System.in);
//        String coMamZrobic;
//
//        while(!(coMamZrobic = scanner.nextLine()).equals("exit"))
//        {
//            if(coMamZrobic.equals("odczyt"))
//            {
//                //odczyt
//                try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file)))
//                {
//                    lista = (List<Samochod>) ois.readObject();
//                }
//                catch (FileNotFoundException e)
//                {
//                    e.printStackTrace();
//                } catch (IOException e)
//                {
//                    e.printStackTrace();
//                } catch (ClassNotFoundException e)
//                {
//                    e.printStackTrace();
//                }
//            }
//            else if(coMamZrobic.equals("zapisz"))
//            {
//                // zapis
//                try(ObjectOutputStream ois = new ObjectOutputStream(new FileOutputStream(file)))
//                {
//                    ois.writeObject(lista);
//                }
//                catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            else
//            {
//                lista.add(new Samochod());
//            }
//        }

//        File file = new File("cms-responsive-content.txt");
//        System.out.println("Plik " + file.getPath() + " : " + (file.length() / (1024.0 * 1024) + " MB"));
//
//        try(BufferedReader in = new BufferedReader(new FileReader(file));)
//        {
//            String line;
//            StringBuilder sb = new StringBuilder();
//            while((line = in.readLine()) != null)
//            {
//                sb.append(line).append("\n");
//            }
//            System.out.println(sb.toString());
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Scanner scanner = new Scanner(System.in);
        String nazwaPliku = scanner.next();

        File odczyt = new File(nazwaPliku);
        File zapis = new File("zapis.txt");
        StringBuilder sb = new StringBuilder();

        try(BufferedReader in = new BufferedReader(new FileReader(odczyt));)
        {
            String line;
            while((line = in.readLine()) != null)
            {
                sb.append(line).append("\n");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        try(PrintWriter out = new PrintWriter(new FileWriter(zapis)))
        {
            zapis.createNewFile();
            out.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*
        Napisz program, który pobierze od użytkownika ścięzkę do pliku tekstowego.
        Niech program pobierze kilka linii z pliku i zapisze je d oinnego pliku.
     */



}
