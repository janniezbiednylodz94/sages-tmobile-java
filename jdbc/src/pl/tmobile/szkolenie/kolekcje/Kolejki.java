package pl.tmobile.szkolenie.kolekcje;

import java.util.*;

public class Kolejki
{
    public static void main(String[] args) {
        Queue<String> queue = new ArrayDeque<String>();
        System.out.println(queue.offer("Osoba"));
        System.out.println(queue.offer("Osoba 2"));
        System.out.println(queue.offer("Osoba 3"));
        System.out.println(queue.poll());
        System.out.println(queue.poll());

        PriorityQueue<String> priorityQueue = new PriorityQueue<String>();
        priorityQueue.addAll(Arrays.asList("c", "d", "e", "z", "a"));
        System.out.println(priorityQueue);

        while (priorityQueue.size() > 0)
        {
            System.out.println(priorityQueue.remove());
            System.out.println(priorityQueue);
        }

        Stack<Integer> stack = new Stack<Integer>();

        System.out.println(stack);
        stack.push(10);
        System.out.println(stack);
        stack.push(15);
        System.out.println(stack);
        stack.push(80);
        System.out.println(stack);

        stack.pop();
        System.out.println(stack);

        stack.isEmpty();
        stack.size();

        for (Iterator<Integer> iteracje = stack.iterator(); iteracje.hasNext();)
        {
            System.out.println(iteracje.next());
        }


    }
}
