package pl.tmobile.szkolenie.kolekcje;

import java.util.*;

public class Kolekcja
{
    public static void main(String[] args)
    {
        // arrayList => [A] [B] [C] [D] [E]
        //               0   1   2   3   4
        String tekst = "Andrzej";
        ArrayList<String> arrayListaStringow = new ArrayList<String>();
        arrayListaStringow.add(tekst);
        arrayListaStringow.add("ma");
        arrayListaStringow.add("szkolenie");
        arrayListaStringow.add("Andrzej");

        /*
            Stworz arrayliste intow.
            Wyswietl je w postaci strumienia.
            Wyswietl tylko te, ktore sa wieksze niz 10.
            Wypisz na ekran.
         */

        ArrayList<String> kopiaPowyzszejArraylist = new ArrayList<>(arrayListaStringow);
        System.out.println("Andrzej jest na pozycji: " + arrayListaStringow.indexOf("Andrzej"));

        for(String zmiennaTymczasowa : arrayListaStringow)
        {
            System.out.println(zmiennaTymczasowa);
        }

        /*for(String s : kopiaPowyzszejArraylist)
        {
            System.out.println("Usuwam: " + s);
            arrayListaStringow.remove(s);
        }*/

//        for(int i = 0; i < arrayListaStringow.size(); i++)
//        {
//            System.out.println(arrayListaStringow.get(i));
//        }

        System.out.println(arrayListaStringow);

        for(Iterator<String> i = arrayListaStringow.iterator(); i.hasNext();)
        {
            i.next();
            i.remove();
        }

        System.out.println(arrayListaStringow);

        // LinkedList => (A) <-> (B) <-> (D) <-> (C)
        List<String> linkedLista = new LinkedList<String>();
        linkedLista.add("Andrzej");
        linkedLista.add("ma");
        linkedLista.add("kota");
        linkedLista.add("Andrzej");

        System.out.println(linkedLista.indexOf("Andrzej"));
        System.out.println(linkedLista.lastIndexOf("Andrzej"));
        System.out.println(linkedLista.isEmpty());
        System.out.println(linkedLista.size());
        System.out.println(linkedLista.contains("Andrzej"));

        List<String> przeksztalcona = new LinkedList<>(arrayListaStringow);

        List<String> nowaListaWLocie = Arrays.asList("a", "b", "c", "d", "d", "c");
        ArrayList arrayList = new ArrayList();

        List<String> owoce = new ArrayList<String>();
        owoce.add("Jablko");
        owoce.add("Gruszka");

        List<String> owoceIWarzywa = new ArrayList<String>();
        owoceIWarzywa.add("Cebula");
        owoceIWarzywa.addAll(owoce);
        System.out.println(owoceIWarzywa);
        owoceIWarzywa.removeAll(owoce);
        System.out.println(owoceIWarzywa);

        Set<String> nowySet = new HashSet<String>();
        nowySet.add("Andrzej");
        nowySet.add("Tomek");
        nowySet.add("Andrzej");
        System.out.println(nowySet);

        Set<String> duzySet = new HashSet<String>();
        duzySet.add("Tomek");
        duzySet.add("Marcin");

        nowySet.addAll(duzySet);
        System.out.println(nowySet);

        for(String imie : duzySet)
        {
            System.out.println(imie);
        }

        for(Iterator<String> iterator = duzySet.iterator(); iterator.hasNext();)
        {
            System.out.println(iterator.next());
        }

        Set<String> oczyszczonaLista = new HashSet<String>(nowaListaWLocie);
        System.out.println(oczyszczonaLista);

        Set<String> treeSet = new TreeSet<String>();
        treeSet.add("Andrzej");
        treeSet.add("Tomasz");
        treeSet.add("Rafal");

        //Set<String> subDrzewko = treeSet.

//        Chair chair = new Chair("ABC", "IKEA", 2020);
//        Chair chair1 = new Chair("ABCD", "IKEA", 2020);
//        Chair chair2 = new Chair("ABC", "IKEA", 2020);
//
//        System.out.println("chair i char1" + chair.equals(chair1));
//        System.out.println("chair i chair2" + chair.equals(chair2));

        //List => pozwalaja na duplikaty, uporzadkowana kolejnosc
        //Set => duplikatow brak, kolejnosc losowa, musimy miec metody hashCode i equals

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("1234", "Andrzej");
        hashMap.put("4343", "Tomek");

        System.out.println(hashMap.get("12345"));


    }
}
