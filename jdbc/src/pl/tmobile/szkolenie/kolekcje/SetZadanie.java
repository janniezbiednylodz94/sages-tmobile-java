package pl.tmobile.szkolenie.kolekcje;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SetZadanie
{
    /*
    Napisz program, który będzie pobierał od użytkownika imiona.
    Program powinien pozwolić użytkownikowi na wprowadzenie
    dowolnej liczby imion
    (wprowadzenie „-” jako imienia przerwie wprowadzanie).
    Na zakończenie wypisz liczbę unikalnych imion.
     */
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        String imie;

        Set<String> imiona = new HashSet<String>();

        do
        {
            imie = scanner.next();

            if(!imie.equals("-"))
            {
                imiona.add(imie);
            }
        }
        while(!imie.equals("-"));

        System.out.println("Ilosc unikalnych imion: " + imiona.size());


    }
}
