package pl.tmobile.szkolenie.kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ZadanieMapy {

    /*
        Napisz program, który będzie pobierał od użytkownika
        imiona par dopóki nie wprowadzi imienia „-”,
        następnie poproś użytkownika o podanie jednego z wcześniej
        wprowadzonych imion i wyświetl imię odpowiadającego mu partnera.
         */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, String> pary = new HashMap<String, String>();

        String imie1;
        String imie2;

        while(true)
        {
            System.out.println("Imie 1: ");
            imie1 = scanner.next();

            if(imie1.equals("-"))
            {
                break;
            }

            System.out.println("Imie 2: ");
            imie2 = scanner.next();

            if(imie2.equals("-"))
            {
                break;
            }

            pary.put(imie1, imie2);
        }

        System.out.println("Poszukaj mnie:");
        imie1 = scanner.next();

        System.out.println(pary.get(imie1));

        /*
        Napisz program, który będzie pobierał od użytkownika imiona.
        Program powinien pozwolić użytkownikowi na wprowadzenie dowolnej liczby imion
        (wprowadzenie „-” jako imienia przerwie wprowadzanie).
        Na zakończenie wypisz liczbę unikalnych imion.
         */
    }
}
