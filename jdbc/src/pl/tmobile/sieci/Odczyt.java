package pl.tmobile.sieci;

import java.io.BufferedReader;
import java.io.IOException;

public class Odczyt implements Runnable
{
    private BufferedReader bufferedReader;

    public Odczyt(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    @Override
    public void run()
    {
        while(true)
        {
            try {
                if(bufferedReader.ready())
                {
                    System.out.println(bufferedReader.readLine());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}