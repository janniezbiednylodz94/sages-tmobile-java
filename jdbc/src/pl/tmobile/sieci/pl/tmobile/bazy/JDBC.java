package pl.tmobile.sieci.pl.tmobile.bazy;

import java.sql.*;

public class JDBC
{
    public static void main(String[] args) throws ClassNotFoundException {
        //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        //jdbc:sqlserver://[serverName[\instanceName][:portNumber]][;property=value[;property=value]]
        String url = "jdbc:sqlserver://localhost:1443;encrypt=false;databaseName=SAGES;user=sa;password=sages";

        try(Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
        )
        {
            String SQL = "SELECT TOP 10 * FROM Person";
            ResultSet resultSet = statement.executeQuery(SQL);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }


}
