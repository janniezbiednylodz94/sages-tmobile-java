package pl.tmobile.sieci;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Klient
{
    public static void main(String[] args) {
        try(Socket socket = new Socket("localhost", 221);
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

            System.out.println("Nawiazalem polaczenie");
            while(true)
            {
                try {
                    Scanner scanner = new Scanner(System.in);
                    String cmd;
                    while(! (cmd = scanner.nextLine()).equals("exit"))
                    {
                        if(cmd.startsWith("write"))
                        {
                            printWriter.println(cmd);
                            printWriter.flush();
                        }
                        else if(cmd.startsWith("read"))
                        {
                            if(bufferedReader.ready())
                            {
                                System.out.println(bufferedReader.readLine());
                            }
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
