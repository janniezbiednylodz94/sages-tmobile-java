package pl.tmobile.interfejsy;

public class Punkt implements Promocyjny, Znizkowy
{
    private int x;
    private int y;

    public Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int policzPromocje()
    {
        return 5;
    }

    @Override
    public void znizka()
    {

    }

    @Override
    public void nowa()
    {

    }
}
