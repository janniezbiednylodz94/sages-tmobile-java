package pl.tmobile.programy;

import pl.tmobile.programy.sportowcy.Koszykarz;
import pl.tmobile.programy.sportowcy.Pilkarz;

public class SuperTrudne
{
    public static void main(String[] args) {
        /*
        Klasy - klasy abstrakcyjne
        Napisz program, który utworzy instancję klas Piłkarz,
        Koszykarz, który dziedziczą z klasy Sportowiec.
        Uporządkuj położenie klas korzystając z pakietów.
        Zastosuj poznane modyfikatory dostępu.
        Spraw, aby klasa Sportowiec pozostała niemożliwa do utworzenia.
        Klasa Sportowiec powinna posiadać metodę abstrakcyjną,
        która zwraca wysokość pensji sportowca.
         */

        Pilkarz pilkarz = new Pilkarz("Jan");
        Koszykarz koszykarz = new Koszykarz("Tomasz");

        System.out.println(pilkarz.getPensja());
        System.out.println(koszykarz.getPensja());



    }
}
