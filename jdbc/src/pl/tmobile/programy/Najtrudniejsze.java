package pl.tmobile.programy;

import pl.tmobile.programy.zwierzeta.Kot;
import pl.tmobile.programy.zwierzeta.Krowa;
import pl.tmobile.programy.zwierzeta.Pies;
import pl.tmobile.programy.zwierzeta.Zwierze;
import pl.tmobile.zadanie.ptaki.Jaskolka;
import pl.tmobile.zadanie.ptaki.Koliber;

import java.util.Scanner;

public class Najtrudniejsze
{
    public static void main(String[] args) {
        /*
        Klasy
        Napisz program, który utworzy instancje dwóch róznych klas.
        Klasy niech przedstawiają ptaki, Jaskółkę oraz Kolibra.
        Uporządkuj położenie klas korzystając z pakietów.
        Pamiętaj o dodaniu atrybutów klasy oraz
        odpowiednich metod jak konstruktor, gettery czy settery.
        Do każdej z klas dołacz metodę, która zachowa się
        w unikatowy dla niej sposób.

         */

        Koliber koliber = new Koliber(30);
        Jaskolka jaskolka = new Jaskolka(100);

        koliber.typowyKolbier();
        jaskolka.typowaJaskolka();

        /*
        Klasy - dziedziczenie, polimorfizm
        Napisz program, który utworzy instancje 3 różnych klas.
        Klasy te będą opisywać takie zwierzęta jak Pies, Kot, Krowa.
        Uporządkuj położenie klas korzystając z pakietów.
        Zastosuj poznane modyfikatory dostępu.
        Spraw, aby powyższe klasy rozszerzały klasę Zwierze,
        która będzie posiadała atrybut imię oraz metodę dajGłos().
        Dla każdego konkretnego zwierzęcia przesłoń metodę dajGłos tak,
        aby przy wywołaniu metody dajGłos na ekranie został wypisany
        odpowiedni dla tego gatunku napis imitujący dzwiek.
        Skorzystaj z poznanego mechanizmu polimorfizmu
        w trakcie tworzenia obiektu. Aby stworzyć obiekt, pobierz
        numer zwierzęcia z klawiatury i na podstawie podanego wyboru,
        utwórz obiekt. Pies - 1 Kot - 2 Krowa - 3
         */
        Scanner scanner = new Scanner(System.in);
        int wybor = scanner.nextInt();

        Zwierze mojeZwierze;

        switch (wybor)
        {
            case 1:
                mojeZwierze = new Pies("Burek");
                break;

            case 2:
                mojeZwierze = new Kot("Mruczek");
                break;

            case 3:
                mojeZwierze = new Krowa("Mucka");
                break;

            default:
                mojeZwierze = null;
        }

        if(mojeZwierze != null)
        {
            mojeZwierze.dajGlos();
        }

    }
}
