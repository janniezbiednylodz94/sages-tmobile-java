package pl.tmobile.programy;

public class Trudniejsze
{
    public static void main(String[] args) {
        /*
        Porównanie
        Napisz program, który porówna dwie liczby całkowite i
        wyświetli komunikat o tym, czy są równe.
        Rozszerz powyższy program o porównanie dwóch napisów.
        */

        //porownanie(2,4);
        System.out.println(porownanie(2,4));

        //porownanie("test", "test");
        System.out.println(porownanie("test", "test"));
        /*

        Metody
        Napisz metodę, która sprawdzi czy temperatura podana
        jako argument jest dodatnia.

        */

        //sprawdzTemperature(10);
        System.out.println(sprawdzTemperature(10));
        /*
        Tablice
        Napisz metodę, która zwróci sumę wszystkich
        elementów tablicy typu int[]
         */

        int tablicaIntow[] = { 3, 5, 10 };
        System.out.println(zsumujTablice(tablicaIntow));
    }

    public static int zsumujTablice(int tablica[])
    {
        int suma = 0;

        for(int numerElementu = 0; numerElementu < tablica.length; numerElementu++)
        {
            suma = suma + tablica[numerElementu];
        }

        return suma;
    }

    public static boolean sprawdzTemperature(double temperatura)
    {
        return (temperatura > 0);
    }

    public static boolean porownanie(int a, int b)
    {
        return (a == b) ? true : false;
    }

    public static boolean porownanie(String a, String b)
    {
        if(a.equals(b))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
