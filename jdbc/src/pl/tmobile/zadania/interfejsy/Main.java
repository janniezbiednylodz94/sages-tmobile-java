package pl.tmobile.zadania.interfejsy;

public class Main
{
    public static void main(String[] args) {
        Obliczenia obliczenia = null;

        int rodzaj = 2;

        if(rodzaj == 1)
        {
            obliczenia = new Dodawanie();
        }

        if(rodzaj == 2)
        {
            obliczenia = new Odejmowanie();
        }

        System.out.println(obliczenia.oblicz(4,3));
    }
}
