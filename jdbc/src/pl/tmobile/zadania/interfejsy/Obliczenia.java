package pl.tmobile.zadania.interfejsy;

/*
        Interfejsy
        Wiedząc, że interfejs Obliczenia ma postać:

        public interface Obliczenia{
            double oblicz(double liczba1, double liczba2);
        }

        napisz 2 klasy Dodawanie oraz Odejmowanie.
        Spraw, aby obie klasy implementowały podany interfejs.
        Uporządkuj położenie klas korzystając z pakietów.
        Zastosuj poznane modyfikatory dostępu.
        Wykorzystaj poznany mechanizm polimorfizmu, aby uzyskać odpowiednią instancję obiektu w zależności
        od pożądanej operacji matematycznej do wykonania.
        Pozyskaną instancję przypisz do referencje o typu
        Obliczenia i wywołaj metodę oblicz dla podanych parametrów.
         */


public interface Obliczenia
{
    double oblicz(double liczba1, double liczba2);
}
