package pl.tmobile.statycznosc;

public class KlasaStatyczna {

    static int ktoryObiekt = 0;
    String nazwa;

    KlasaStatyczna(String nazwa)
    {
        this.nazwa = nazwa;
        System.out.println(++ktoryObiekt);
    }

    public static void nowaMetoda()
    {
        System.out.println("Jestem nowa metoda");
    }

    public String zmienNazwe(String nowaNazwa)
    {
        return this.nazwa = nowaNazwa;
    }
}
