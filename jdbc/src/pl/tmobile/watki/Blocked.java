package pl.tmobile.watki;

class Licznik
{
    private volatile int wartosc;

    public synchronized void zwieksz()
    {
        wartosc += 1;
    }

    public int getWartosc()
    {
        return wartosc;
    }
}

public class Blocked
{
    public static void main(String[] args) throws InterruptedException {
        Licznik licznik = new Licznik();
        Runnable r = () -> {
            for(int i = 0; i < 1000; i++)
            {
                licznik.zwieksz();
            }
        };

        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        Thread t3 = new Thread(r);

        t1.start();
        t2.start();
        t3.start();

        t1.join(); // 1000
        t2.join(); // 738
        t3.join(); // 1000

        System.out.println(licznik.getWartosc());

    }
}
