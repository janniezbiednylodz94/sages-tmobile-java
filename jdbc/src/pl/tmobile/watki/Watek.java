package pl.tmobile.watki;

public class Watek
{
    public static void main(String[] args) {
//        Thread thread = new NowyWyatek();
//        Thread thread1 = new Thread(new RunnableWatek());
//        Thread thread2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                    System.out.println("Runnable w locie");
//            }
//        });
//        Thread thread3 = new Thread(() -> System.out.println("Jestem run"));
//        // NEW -> RUNNABLE -> TERMINATED
//        // RUNNABLE -> WAITING
//        // RUNNABLE -> BLOCKED
//        // RUNNABLE -> TIMED_WAITING
//        thread.start();
//        thread1.start();
//        thread2.start();
//        thread3.start();
//        thread.start();
        System.out.println("Bomba start");
        Thread thread = new Thread(() -> {
            System.out.println("T0 - odliczanie");
            for(int i=0; i < 5; i++)
            {
                System.out.println("T = " + i);
            }
            System.out.println("BOOM");
        });
        thread.start();

        for(int i=0; i<5; i++)
        {
            System.out.println("Moj czas: " + i);
        }
        System.out.println("MC stop");

    }
}
