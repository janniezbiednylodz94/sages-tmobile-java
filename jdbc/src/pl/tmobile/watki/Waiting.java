package pl.tmobile.watki;

import java.time.Duration;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class Waiting
{
    private static final Random random = new Random();
    private static final Queue<String> queue = new LinkedList<>();

    public static void main(String[] args) {
        int itemCount = 5;

        Thread producent = new Thread(() -> {
           for(int i = 0; i < itemCount; i++)
           {
               try {
                   Thread.sleep(Duration.ofSeconds(random.nextInt(5)).toMillis());
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               synchronized(queue)
               {
                   if(i == 4)
                   {
                       queue.add("Auto nr: " + i + "LAST");
                   }
                   else
                   {
                       queue.add("Auto nr: " + i);
                   }

                   queue.notify();
               }
           }
        });

        // Object.notify()
        // Object.wait()
        // Object.notifyAll();

        Thread konsument = new Thread(() -> {
            int ileZostalo = itemCount;
            while(true)
            {
                String item;
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    item = queue.poll();
                }


                System.out.println(item);

                if(item.endsWith("LAST"))
                {
                    break;
                }
            }

//            while(ileZostalo > 0)
//            {
//                String item;
//                synchronized (queue) {
//                    while (queue.isEmpty()) {
//                        try {
//                            queue.wait();
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    item = queue.poll();
//                }
//                ileZostalo--;
//                System.out.println(item);
//            }
        });

        producent.start();
        konsument.start();
    }


}
