package pl.tmobile.wewnetrzne;

public class Koralik2
{
    private int wartoscKoralika;
    private Koralik2 nastepnyKoralik;

    public Koralik2(int wartosc)
    {
        this.wartoscKoralika = wartosc;
    }

    public int getWartoscKoralika() {
        return wartoscKoralika;
    }

    public void setWartoscKoralika(int wartoscKoralika) {
        this.wartoscKoralika = wartoscKoralika;
    }

    public Koralik2 getNastepnyKoralik() {
        return nastepnyKoralik;
    }

    public void setNastepnyKoralik(Koralik2 nastepnyKoralik) {
        this.nastepnyKoralik = nastepnyKoralik;
    }
}
