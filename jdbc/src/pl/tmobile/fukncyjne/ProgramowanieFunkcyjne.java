package pl.tmobile.fukncyjne;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.*;

public class ProgramowanieFunkcyjne
{
    public static void main(String[] args) {

//        WyrazeniaLambda wyrazeniaLambda = (x, y) -> {
//            System.out.println(x);
//            System.out.println(y);
//            System.out.println(x + y);
//        };
//        wyrazeniaLambda.wyrazenie(3, 4);
//
//        Stream<String> owoce = Stream.of("jalbko", "banan", "kiwi");
//        owoce.filter(owoc -> owoc.contains("a")).forEach(System.out::println);
//
//        Gra gra = new Gra("Call of Duty", "OK", 3);
//        Gra gra2 = new Gra("Call of Duty 2", "OK", 4);
//        Gra gra4 = new Gra("Call of Duty 8", "OK", 4);
//        Gra gra3 = new Gra("Call of Duty 3", "NOT OK", 3);
//
//        List<Gra> lista = new ArrayList<Gra>();
//        lista.add(gra);
//        lista.add(gra2);
//        lista.add(gra3);
//        lista.add(gra4);
//
//        for(Gra graa : lista)
//        {
//            if(graa.ocena.equals("OK"))
//            {
//                if(graa.cena > 3)
//                {
//                    System.out.println(graa.nazwa.toUpperCase(Locale.ROOT));
//                }
//            }
//        }
//
//        //Stream<Gra> gry = lista.stream();
//
//        /*
//            COD, COD2, COD3
//
//            public boolean czyRowne(Stiring g, String wartosc)
//            {
//                return g.equals(wartosc);
//            }
//
//            COD, COD2
//
//
//
//            COD2
//         */
//
//        lista.stream()
//                .filter(g -> g.ocena.equals("OK"))
//                .filter(g -> g.cena > 3)
//                .map(g -> g.nazwa.toUpperCase(Locale.ROOT)).
//                limit(1)
//                //.forEach(System.out::println);
//                .count();
//
//        Stream<Integer> streamKolekcja = new LinkedList<Integer>().stream();
//        Stream<Integer> streamTablica = Arrays.stream(new Integer[]{});
//        DoubleStream doubleStream = DoubleStream.of(1, 2, 3);
//        IntStream intStream = IntStream.of(0 , 1 , 3);
//
//        System.out.println("Loteria:");
//        LongStream longStream = new Random().longs();
//        longStream.limit(3).forEach(System.out::println);
//
//        List<String> stringi = Arrays.asList("aaa", "ab", "bc", "d", "d");
//        String nowyString =
//                stringi.stream()
//                        .map(string -> string.toUpperCase(Locale.ROOT)).collect(Collectors.joining(", "));
//
//        System.out.println(nowyString);
//
//        List<Integer> listaIntow = Arrays.asList(9, 10, 3, 4 ,3, 3, 4);
//        List<Integer> nowa = listaIntow.stream().distinct().collect(Collectors.toList());
//
//        for(Integer s : nowa)
//        {
//            System.out.println(s);
//        }
//
//        /*
//            Stworz arrayliste intow.
//            Wyswietl je w postaci strumienia.
//            Wyswietl tylko te, ktore sa wieksze niz 10.
//            Wypisz na ekran.
//         */

        List<Integer> listaIntegerow = Arrays.asList(9, 11, 3, 4 ,3, 3, 4);
        listaIntegerow.stream()
                .filter(liczba -> liczba > 10)
                .forEach(System.out::println);

        //System.out.println(ileWiekszych);

        //Function<T, R> -> apply, T to co wchodzi do lambdy, R to co wychodzi z lambdy jako wynik
        //Consumer<T> -> accept, T to co wchodzi
        //Predicate<T> -> test, T to co wchodzi, lambda zwraca true lub false
        //Supplier<T> -> get, lambda nie przyjmuje obiektu, ale zwraca typ T
        //UnaryOperator<T> -> apply, T jest takie samo jak R, to co wchodzi i wychodzi takie samo

        WyrazeniaLambda<Integer, Integer> predykat = (x, y) -> x > 10;
        System.out.println(predykat.wyrazenie(15, 10));

        UnaryOperator<Integer> doKwadratu = x -> x * x;
        System.out.println(doKwadratu.apply(3));

        Supplier<String> napis = () -> "Napis";
        System.out.println(napis.get());

        BiConsumer<Integer, Long> dodawanie = (Integer a, Long b) -> System.out.println(a + b);
        dodawanie.accept(3, 5L);

        Function<Integer, Long> czaryMary = jeden ->
        {
            if(jeden > 0)
            {
                return (long) jeden * jeden;
            }
            else
            {
                return 1L;
            }
        };

        System.out.println(czaryMary.apply(5));







    }
}
