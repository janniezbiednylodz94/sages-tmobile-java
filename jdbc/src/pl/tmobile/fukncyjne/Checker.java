package pl.tmobile.fukncyjne;

@FunctionalInterface
public interface Checker<T>
{
    boolean check(T object);
}
