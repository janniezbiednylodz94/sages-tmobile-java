package pl.tmobile.wyliczenia;

public enum TshirtSize
{
    S(10, 20, 30),
    M(15, 25, 35),
    L(25, 35, 45),
    XL(55, 65, 75);

    private int szerokoscKlatki;
    private int dlugosc;
    private int dlugoscRekawa;

    TshirtSize(int klatka, int dlugosc, int rekaw)
    {
        this.szerokoscKlatki = klatka;
        this.dlugosc = dlugosc;
        this.dlugoscRekawa = rekaw;
    }

    public int getSzerokoscKlatki() {
        return szerokoscKlatki;
    }

    public int getDlugosc() {
        return dlugosc;
    }

    public int getDlugoscRekawa() {
        return dlugoscRekawa;
    }
}
