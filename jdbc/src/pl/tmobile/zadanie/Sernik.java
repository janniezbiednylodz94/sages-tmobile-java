package pl.tmobile.zadanie;

public class Sernik extends Ciasto
{
    boolean czyMaRodzynki;

    public Sernik(String nazwaCiasta, boolean czyMaRodzynki) {
        super(nazwaCiasta);
        this.czyMaRodzynki = czyMaRodzynki;
    }

    public boolean isCzyMaRodzynki() {
        return czyMaRodzynki;
    }

    public void setCzyMaRodzynki(boolean czyMaRodzynki) {
        this.czyMaRodzynki = czyMaRodzynki;
    }

    public void usunRodzynki()
    {
        this.czyMaRodzynki = false;
        System.out.println("Rodzynki usuniete");
    }

}
